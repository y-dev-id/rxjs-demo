import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PostContainerComponent } from './containers/post-container/post-container.component';
import { PostComponent } from './components/post/post.component';
import { UserComponent } from './components/user/user.component';
import {ReactiveFormsModule} from '@angular/forms';
import { PostContainer2Component } from './containers/post-container/post-container2.component';
import {PostContainer3Component} from './containers/post-container/post-container3.component';
import { PostLoadingErrorContainerComponent } from './containers/post-loading-error-container/post-loading-error-container.component';
import {PostLoadingErrorContainer2Component} from './containers/post-loading-error-container/post-loading-error-container2.component';
import { PostWithUserContainerComponent } from './containers/post-with-user-container/post-with-user-container.component';
import {PostWithUserContainer2Component} from './containers/post-with-user-container/post-with-user-container.component2';
import {PostWithUserContainer3Component} from './containers/post-with-user-container/post-with-user-container.component3';
import {PostWithUserContainer4Component} from './containers/post-with-user-container/post-with-user-container.component4';
import {PostWithUserContainer5Component} from './containers/post-with-user-container/post-with-user-container.component5';

@NgModule({
  declarations: [
    AppComponent,
    PostComponent,
    UserComponent,
    PostContainerComponent,
    PostContainer2Component,
    PostContainer3Component,
    PostLoadingErrorContainerComponent,
    PostLoadingErrorContainer2Component,
    PostWithUserContainerComponent,
    PostWithUserContainer2Component,
    PostWithUserContainer3Component,
    PostWithUserContainer4Component,
    PostWithUserContainer5Component,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FlexLayoutModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
