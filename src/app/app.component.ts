import { Component } from '@angular/core';
import {BehaviorSubject, forkJoin, interval, Observable, of, ReplaySubject, Subject, Subscription} from 'rxjs';
import {filter, map, mergeMap, tap} from 'rxjs/operators';
import {DemoService} from './services/demo.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  componentToShow: null|'c1'|'c2'|'c3'|'c4'|'c5' = null;

  advancedMulticastComponentToShow: null|'c1'|'c2'|'c3'|'c4'|'c5' = null;

  constructor(private demoService: DemoService) {
  }

  /**
   * Exemple si on ne subscribe pas, l'Observable étant "cold" rien ne se passera
   */
  basicSansSubscribe() {
    console.log('Before getTodo()');
    const getTodo: Observable<ToDo> = this.demoService.getTodo();
    console.log('After getTodo()', getTodo);
  }

  /**
   * Exemple avec subscribe, l'appel réseau est bien déclenché
   */
  basicAvecSubscribe() {
    console.log('Before getTodo()');
    const subscription: Subscription = this.demoService.getTodo().subscribe();
    console.log('After getTodo()', subscription);
  }

  /**
   * Les promises sont des observables "hot" et se déclenchent même sans subscribe
   */
  basicPromise() {
    const fetchPromise: Promise<Response> = fetch('https://jsonplaceholder.typicode.com/todos/1');
    console.log(fetchPromise);
    const todoPromise: Promise<ToDo> = this.demoService.getTodo().toPromise();
    console.log(todoPromise);
  }

  /**
   * Exemple avec subscribe, subscribe prend 3 fonctions :
   * -Next qui enverra les valeurs au fur et à mesure
   * -Error qui se déclenche si une erreur survient
   * -Complete qui ne fournit pas de valeur, mais annonce que l'observable n'émettra plus de valeur
   *
   * Pour résumer, un Observable peut appeler 0 ou n fois la fonction Next et 0 ou 1 fois error ou complete
   * Il faut bien noter que les 2 sont exclusifs, on aura soit error, soit complete et l'observable sera terminé
   * Le fait que la moindre error termine l'Observable est très important à garder en tête
   */
  basicAvecSubscribeEtResultat() {
    console.log('Before getTodo()');
    const subscription: Subscription = this.demoService.getTodo().subscribe(todo => {
      console.log('Inside getTodo()', todo);
    }, error => {
      console.error('Inside getTodo() error', error);
    }, () => {
      console.log('Inside getTodo() complete');
    });
    console.log('After getTodo()', subscription);
  }

  /**
   * Exemple d'un observable en error
   */
  basicAvecSubscribeEtError() {
    console.log('Before getUser()');
    const subscription: Subscription = this.demoService.getUser(-1).subscribe(user => {
      console.log('Inside getUser()', user);
    }, error => {
      console.error('Inside getUser() error', error);
    }, () => {
      console.log('Inside getUser() complete');
    });
    console.log('After getUser()', subscription);
  }


  /**
   * Exemple avec la fonction next déclenchée plusieurs fois
   * Dans le cas d'un appel réseau, une fois l'appel terminé, la source est épuisée et donc l'observable se termine
   * Ici on a un observable qui emet 5 valeurs avant de se terminer
   */
  basicAvecSubscribeEtResultatPlusieursNext() {
    console.log('Before subscribe()');
    const subscription: Subscription = of(1, 2, 3, 4, 5).subscribe(i => {
      console.log('Inside next', i);
    }, error => {
      console.error('Inside error', error);
    }, () => {
      console.log('Inside complete');
    });
    console.log('After subscribe()', subscription);
  }

  /**
   * Exemple avec une source infinie : interval émet une valeur toutes les x ms
   * On voit l'utilité de la subscription, sans mettre fin à la subscription l'observable émettra infiniment
   *
   * Il est important de noter que les observables ne sont pas scopés au cycle de vie des composants
   * et peuvent donc créer des fuites de mémoire si on unsuscribe pas
   */
  basicAvecSubscribeEtResultatPlusieursNextUnsuscribe() {
    console.log('Before subscribe()');
    const subscription: Subscription = interval(1000).subscribe(i => {
      console.log('Inside next', i);
      if (i === 4) {
        console.log('Unsubscribing...');
        subscription.unsubscribe();
      }
    }, error => {
      console.error('Inside error', error);
    }, () => {
      console.log('Inside complete');
    });
    console.log('After subscribe()', subscription);
  }

  /**
   * Exemple d'enchaînement de requêtes, en utilisant les opérations de base
   * ça devient extrêmement complexe de suivre les appels, gérer la synchronisation
   * et les erreurs
   */
  basicEnchainementDeRequetes() {
    console.log('Before getPost()');
    const subscription: Subscription = this.demoService.getPost(1)
      .subscribe(post => {
      console.log('Dans getPost()', post);
      const subscription2 = this.demoService.getUser(post.userId).subscribe(user => {
        console.log('Inside getUser', user);
      });
      console.log('After getUser()');
      const subscription3 = this.demoService.getComments(post.id).subscribe(comments => {
        console.log('Inside getComments', comments);
      });
    });
    console.log('After getPost()', subscription);
  }

  /**
   * On va commencer par un cas simple où on transforme simplement un observable en un autre
   */
  basicCountComments() {
    // Je n'ai pas subscribe donc pour le moment rien n'est exécuté
    const commentsObservable: Observable<Comment[]> = this.demoService.getComments(1);
    // Je transforme un observable qui renvoie un array de comments en number
    // Cet opérateur map ressemble à celui des arrays, mais c'est bien un opérateur différent spécifique RxJS
    const countCommentsObservable: Observable<number> = commentsObservable.pipe(
      map(comments => comments.length)
    );
    // Là encore, je n'ai pas subscribe, donc il ne se passe rien
    // Pipe permet de transformer un observable en un autre
    console.log('Avant subscribe countCommentsObservable');
    countCommentsObservable.subscribe(count => console.log('Count = ', count));
    console.log('Avant subscribe commentsObservable');
    commentsObservable.subscribe(comments => console.log('Comments = ', comments));
    // A noter que dans ce cas là, 2 appels réseaux seront émis
    // C'est dû au fait que les observables sont "cold" : tout se déclenche quand on subscribe dessus
    // et par défaut, il n'y a pas de "mémoire" ou d'état
  }

  /**
   * Comme on trouve le map, on peut trouver le filter qui existe sur les arrays et existe aussi en RxJS
   */
  basicFilter() {
    // Il faut bien noter que même si l'observable émet plusieurs number, il en émet qu'un seul à la fois
    const numbers: Observable<number> = of(1, 2, 3, 4, 5);
    // Le filter de RxJS va donc soit prendre, soit annuler entièrement une émission
    const eventNumbers: Observable<number> = numbers.pipe(
      filter(i => i % 2 === 0)
    );
    eventNumbers.subscribe(i => console.log(i));
  }

  /**
   * Attention si on veut filtrer le contenu d'une émission, on doit passer par un map
   * puisque ça revient à transformer la valeur initiale en une autre valeur
   * et non à laisser passer ou bloquer l'émission
   */
  basicFilterComments() {
    const commentsObservable: Observable<Comment[]> = this.demoService.getComments(1);
    const filteredCommentsObservable: Observable<Comment[]> = commentsObservable.pipe(
      filter(comments => {
        // Filter doit renvoyer un boolean, donc on a le choix soit de laisser passer tous les commentaires, soit aucun
        return true;
      }),
      tap(comments => {
        // Tap est un opérateur qui permet de regarder l'observable
        // On peut regarder les valeurs émises (next), mais aussi les erreurs et le complete
        console.log('Avant map ', comments);
      }),
      map(comments => {
        // En revanche, map me permet de transformer un Observable<Comment[]> en un autre Observable<Comment[]>
        // qui contiendra moins de données (et sur l'array on utilise effectivement filter)
        return comments.filter(comment => comment.id === 2);
      }),
      tap(comments => {
        console.log('Après map ', comments);
      })
    );
    // Au passage, on voit qu'on peut enchaîner plusieurs opérateurs
    // En réalité, un opérateur RxJS est simplement une fonction qui renvoie un nouvel observable
    // Cet observable sera celui qui subscribe à la source une fois qu'on l'active
    filteredCommentsObservable.subscribe(comments => console.log(comments));
  }

  /**
   * Tentative d'enchaînement de requêtes avec map
   * Le but est de récupérer l'auteur du post
   */
  basicEnchainementDeRequetesFail() {
    const postObservable: Observable<Post> = this.demoService.getPost(1);
    const userObservable = postObservable.pipe(
      map(post => {
        return this.demoService.getUser(post.userId);
      })
    );
    // Ici on a un soucis car notre userObservable est de type Observable<Observable<User>>
    // En effet, on a transformer le Post en Observable<User> grâce à map
    // On a donc en sortie un Observable<Observable<User>>
    userObservable.subscribe(user => {
      console.log('User = ', user);
    });
  }

  /**
   * L'opérateur mergeMap (flatMap) va subscribe à l'observable et nous renvoyer le résultat
   * On obtient donc le résultat correct comparé à l'exemple précédent
   */
  basicMergeMap() {
    const postObservable: Observable<Post> = this.demoService.getPost(1);
    const userObservable: Observable<User> = postObservable.pipe(
      mergeMap(post => {
        return this.demoService.getUser(post.userId);
      })
    );
    userObservable.subscribe(user => {
      console.log('User = ', user);
    });
  }

  /**
   * L'opérateur forkJoin permet de subscribe à plusieurs observers et d'envoyer le résultat
   * comme si c'était un unique observer
   */
  mergeMapForkJoin() {
    const postObservable: Observable<Post> = this.demoService.getPost(1);
    const fullPostObservable = postObservable.pipe(
      mergeMap(post => {
        // const postAsObservable: Observable<Post> = of(post);
        const userObservable: Observable<User> = this.demoService.getUser(post.userId);
        const commentsObservable: Observable<Comment[]> = this.demoService.getComments(post.id);
        return forkJoin([userObservable, commentsObservable]).pipe(
          map(([user, comments]) => {
            return [post, user, comments];
          })
          // Syntaxe de destructuring JS
          // sans cette syntaxe j'aurais dû faire
          // map(res => {
          //   const user = res[0];
          //   const comments = res[1];
          //   return [post, user, comments];
          // })
        );
      })
    );
    fullPostObservable.subscribe(([post, user, comments]) => {
      // Ici on a un gros avantage, c'est qu'il n'y a pas d'état intermédiaire
      // On a reçu toutes les données ensemble, on assure une cohérence totale des données
      // Si on était passé par des variables intermédiaires stockées dans les propriétés du composant
      // il nous aurait été compliqué/impossible de garantir cette cohérence
      console.log('Post = ', post);
      console.log('User = ', user);
      console.log('Comments = ', comments);
    });
  }

  /**
   * La même chose que la précédente mais "flatten"
   * Il est préférable d'utiliser cette façon plutôt que la précédente pour éviter d'avoir
   * une imbrication trop importante dans les cas plus complexes
   */
  mergeMapForkJoinOf() {
    const postObservable: Observable<Post> = this.demoService.getPost(1);
    const fullPostObservable = postObservable.pipe(
      mergeMap(post => {
        const postAsObservable: Observable<Post> = of(post);
        const userObservable: Observable<User> = this.demoService.getUser(post.userId);
        const commentsObservable: Observable<Comment[]> = this.demoService.getComments(post.id);
        return forkJoin([postAsObservable, userObservable, commentsObservable]);
      })
    );
    fullPostObservable.subscribe(([post, user, comments]) => {
      console.log('Post = ', post);
      console.log('User = ', user);
      console.log('Comments = ', comments);
    });
  }

  /**
   * On monte d'un niveau, il faut que les exemples précédents soient bien compris.
   * On a vu tout à l'heure que les Observables étaient "cold" et donc qu'à chaque
   * subscribe toute la chaîne était déclenchée.
   * Les subjects permettent de faire ce qu'on appelle du multicast, ils peuvent avoir plusieurs
   * subscribers
   */
  basicSubject() {
    const postSubject = new Subject<Post>();
    // Le subject est à la fois un Observable et un "Observer"
    const postObservable: Observable<Post> = postSubject;
    const subscription = postObservable.subscribe(post => {
      console.log('Inside postSubject next', post);
    }, error => {
      console.error('Inside postSubject error', error);
    }, () => {
      console.log('postSubject complete');
    });
    postObservable.subscribe(post => {
      console.log('Inside postSubject2 next', post);
    });
    postSubject.next({
      id: 999,
      userId: 99,
      title: 'Subject Multicast',
      body: 'test',
    });
    // Cet Observer arrive après le next, il a raté la précédente, mais verra les suivantes
    postObservable.subscribe(post => {
      console.log('Inside postSubject3 next', post);
    });
    console.log('Before complete', subscription.closed);
    // Si je n'appelle pas complete, le Subject reste actif et toutes ses subscriptions aussi !
    postSubject.complete();
    console.log('After complete', subscription.closed);
  }

  /**
   * Dans l'exemple précédent, le 3e observer n'a jamais reçu de next car il est arrivé après.
   * Le ReplaySubject permet de garder les x dernières valeurs en mémoire et de les envoyer aux nouveaux Observers
   */
  basicReplaySubject() {
    const postReplaySubject = new ReplaySubject<Post>();
    const subscription = postReplaySubject.subscribe(post => {
      console.log('Inside postReplaySubject next', post);
    }, error => {
      console.error('Inside postReplaySubject error', error);
    }, () => {
      console.log('postReplaySubject complete');
    });
    postReplaySubject.subscribe(post => {
      console.log('Inside postReplaySubject2 next', post);
    });
    postReplaySubject.next({
      id: 999,
      userId: 99,
      title: 'Subject Multicast',
      body: 'test',
    });
    // Grâce au replaySubject cet Observer recevra correctement les données
    postReplaySubject.subscribe(post => {
      console.log('Inside postReplaySubject3 next', post);
    });
    console.log('Before complete', subscription.closed);
    postReplaySubject.complete();
    console.log('After complete', subscription.closed);
    // Le replaySubject va garder en mémoire les éléments, même terminé, un Observer recevra les informations
    postReplaySubject.subscribe(post => {
      console.log('Inside postReplaySubject4 next', post);
    });
  }

  /**
   * Le BehaviorSubject permet de rendre une variable Observable.
   * On est obligé de donner une valeur initiale au BehaviorSubject et contrairement aux autres Observables
   * on peut accéder à sa valeur grâce à .value
   */
  basicBehaviorSubject() {
    const behaviorSubject = new BehaviorSubject<Post>({
      id: 999,
      userId: 99,
      title: 'Subject Multicast',
      body: 'test',
    });
    behaviorSubject.subscribe(post => {
      console.log('Inside behaviorSubject', post);
    });
    // Accès à la valeur sans subscribe
    console.log('behaviorSubject.value', behaviorSubject.value);
    // Si on supprime le fait qu'on puisse accéder à va valeur grâce à .value
    // et qu'on soit forcé de lui donner une valeur initiale, il serait équivalent à ReplaySubject(1)
  }


}

export interface User {
  id: number;
  name: string;
  username: string;
  email: string;
}

export interface ToDo {
  id: number;
  userId: number;
  title: string;
  completed: boolean;
}

export interface Post {
  id: number;
  userId: number;
  title: string;
  body: string;
}

export interface Comment {
  id: number;
  postId: number;
  name: string;
  email: string;
  body: string;
}
