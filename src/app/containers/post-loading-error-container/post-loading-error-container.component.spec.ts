import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostLoadingErrorContainerComponent } from './post-loading-error-container.component';

describe('PostLoadingErrorContainerComponent', () => {
  let component: PostLoadingErrorContainerComponent;
  let fixture: ComponentFixture<PostLoadingErrorContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostLoadingErrorContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostLoadingErrorContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
