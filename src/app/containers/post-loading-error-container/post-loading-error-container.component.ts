import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {Post, User} from '../../app.component';
import {DemoFacade} from '../../services/demo.facade';

@Component({
  selector: 'app-post-loading-error-container',
  templateUrl: './post-loading-error-container.component.html',
  styleUrls: ['./post-loading-error-container.component.scss']
})
export class PostLoadingErrorContainerComponent implements OnInit {

  postIdFormControl = new FormControl(null);

  isLoading$: Observable<boolean>;
  error$: Observable<string|null>;
  post$: Observable<Post>;
  user$: Observable<User>;

  constructor(
    private demoFacade: DemoFacade,
  ) { }

  ngOnInit(): void {
    // Je subscribe à la "chaîne réactive"
    this.demoFacade.loadActivePost().subscribe(
      () => console.log('loadActivePost next'),
      () => console.error('loadActivePost error'),
      () => console.log('loadActivePost complete'),
    );
    this.postIdFormControl.valueChanges.subscribe(
      postId => this.demoFacade.activePostId$.next(postId),
      error => {},
      () => console.log('valueChanges complete'),
    );
    this.isLoading$ = this.demoFacade.isLoading$;
    this.error$ = this.demoFacade.error$;
    this.post$ = this.demoFacade.activePost$;
    this.user$ = this.demoFacade.activeUser$;
  }

  onRetry() {
    this.demoFacade.retryLoadActivePost();
  }
}
