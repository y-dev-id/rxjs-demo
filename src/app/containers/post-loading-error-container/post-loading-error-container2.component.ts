import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {EMPTY, Observable, Subscription, timer} from 'rxjs';
import {Post, User} from '../../app.component';
import {DemoFacade} from '../../services/demo.facade';
import {debounce, materialize, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-post-loading-error-container2',
  templateUrl: './post-loading-error-container.component.html',
  styleUrls: ['./post-loading-error-container.component.scss']
})
export class PostLoadingErrorContainer2Component implements OnInit, OnDestroy {

  postIdFormControl = new FormControl(null);

  isLoading$: Observable<boolean>;
  error$: Observable<string|null>;
  post$: Observable<Post>;
  user$: Observable<User>;

  private subscription = new Subscription();

  constructor(
    private demoFacade: DemoFacade,
  ) { }

  ngOnInit(): void {
    // Ne pas oublier d'unsubscribe !
    this.subscription.add(
      this.demoFacade.loadActivePost().subscribe(
        () => console.log('loadActivePost next'),
        () => console.error('loadActivePost error'),
        () => console.log('loadActivePost complete'),
      )
    );
    this.postIdFormControl.valueChanges.subscribe(
      postId => this.demoFacade.activePostId$.next(postId),
      error => {},
      () => console.log('valueChanges complete'),
    );
    this.isLoading$ = this.demoFacade.isLoading$.pipe(
      debounce(loading => loading ? timer(500) : EMPTY),
      startWith(true),
    );
    this.error$ = this.demoFacade.error$;
    this.post$ = this.demoFacade.activePost$.pipe(
      debounce(post => !!post ? EMPTY : timer(500)),
    );
    this.user$ = this.demoFacade.activeUser$.pipe(
      debounce(user => !!user ? EMPTY : timer(500)),
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onRetry() {
    this.demoFacade.retryLoadActivePost();
  }
}
