import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostWithUserContainerComponent } from './post-with-user-container.component';

describe('PostWithUserContainerComponent', () => {
  let component: PostWithUserContainerComponent;
  let fixture: ComponentFixture<PostWithUserContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostWithUserContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostWithUserContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
