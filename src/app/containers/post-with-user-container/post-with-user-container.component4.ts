import { Component, OnInit } from '@angular/core';
import {DemoFacade, PostWithUser} from '../../services/demo.facade';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-post-with-user-container4',
  templateUrl: './post-with-user-container.component.html',
  styleUrls: ['./post-with-user-container.component.scss']
})
export class PostWithUserContainer4Component implements OnInit {

  postWithUser$: Observable<PostWithUser>;

  constructor(
    private demoFacade: DemoFacade,
  ) { }

  ngOnInit(): void {
    this.postWithUser$ = this.demoFacade.activePostWithUserPublishReplayRefCount$;
  }

}
