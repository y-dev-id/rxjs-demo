import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable, of} from 'rxjs';
import {Post, User} from '../../app.component';
import {DemoService} from '../../services/demo.service';
import {catchError, filter, share, switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-post-container3',
  templateUrl: './post-container.component.html',
  styleUrls: ['./post-container.component.scss']
})
export class PostContainer3Component implements OnInit {

  postIdFormControl = new FormControl(null);

  post$: Observable<Post>;
  user$: Observable<User>;

  constructor(
    private demoService: DemoService,
  ) { }

  ngOnInit(): void {
    this.post$ = this.postIdFormControl.valueChanges.pipe(
      switchMap(postId => this.demoService.getPost(postId).pipe(
        // Catch error va intercepter l'erreur pour la transformer en autre chose
        // Le switchMap ne voit jamais l'erreur passer
        catchError(error => {
          console.error('getPost error!', error);
          // Si l'appel réseau échoue, on va faire passer null à la place
          return of(null);
        }),
      )),
      share(),
    );
    this.user$ = this.post$.pipe(
      // Vu qu'on sait que null peut arriver en cas d'erreur, on va éviter les soucis
      filter(post => !!post),
      // Attention à l'emplacement du catchError ! Ici je fais exprès de créer une erreur si l'id est 2
      switchMap(post => this.demoService.getUser(post.userId === 2 ? -1 : post.userId)),
      // Comme j'ai mis le catchError à la sortie du switchMap, le switchMap aura émis l'erreur
      // On se souvient, un Observable ne peut émettre au maximum 1 erreur ou 1 complete
      // L'observable sera donc KO et mon catchError n'aura pas été très utile !
      catchError(error => {
        console.error('getUser error!', error);
        return of(null);
      })
    );
    // Problème : difficile de différencier un cas de chargement ou d'erreur
  }


}
