import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {Post, User} from '../../app.component';
import {switchMap} from 'rxjs/operators';
import {DemoService} from '../../services/demo.service';

@Component({
  selector: 'app-post-container',
  templateUrl: './post-container.component.html',
  styleUrls: ['./post-container.component.scss']
})
export class PostContainerComponent implements OnInit {

  postIdFormControl = new FormControl(null);

  post$: Observable<Post>;
  user$: Observable<User>;

  constructor(
    private demoService: DemoService,
  ) { }

  ngOnInit(): void {
    // valueChanges est en fait un Subject
    this.post$ = this.postIdFormControl.valueChanges.pipe(
      switchMap(postId => this.demoService.getPost(postId)),
    );
    this.user$ = this.post$.pipe(
      switchMap(post => this.demoService.getUser(post.userId)),
    );
    // Plusieurs problèmes :
    // -2 appels à post (pas de multicast)
    // -Si erreur tout est cassé
  }

}
