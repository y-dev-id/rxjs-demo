import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {Post, User} from '../../app.component';
import {DemoService} from '../../services/demo.service';
import {share, switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-post-container2',
  templateUrl: './post-container.component.html',
  styleUrls: ['./post-container.component.scss']
})
export class PostContainer2Component implements OnInit {

  postIdFormControl = new FormControl(null);

  post$: Observable<Post>;
  user$: Observable<User>;

  constructor(
    private demoService: DemoService,
  ) { }

  ngOnInit(): void {
    this.post$ = this.postIdFormControl.valueChanges.pipe(
      switchMap(postId => this.demoService.getPost(postId)),
      share(),
    );
    // Share crée en fait un Subject qui s'abonne à l'observable
    // Attention c'est un Subject standard, ce qui signifie que si
    // un Observer s'abonne tardivement, il peut rater des événements !
    this.user$ = this.post$.pipe(
      switchMap(post => this.demoService.getUser(post.userId)),
    );
    // Problème : Si erreur tout est cassé
  }


}
