import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Comment, Post, ToDo, User} from '../app.component';

@Injectable({
  providedIn: 'root'
})
export class DemoService {
  constructor(
    private http: HttpClient,
  ) { }


  getTodo(): Observable<ToDo> {
    return this.http.get<ToDo>('https://jsonplaceholder.typicode.com/todos/1');
  }

  getUser(userId: number): Observable<User> {
    return this.http.get<User>(`https://jsonplaceholder.typicode.com/users/${userId}`);
  }

  getPost(postId: number): Observable<Post> {
    return this.http.get<Post>(`https://jsonplaceholder.typicode.com/posts/${postId}`);
  }

  getComments(postId: number): Observable<Comment[]> {
    return this.http.get<Comment[]>(`https://jsonplaceholder.typicode.com/posts/${postId}/comments`);
  }
}
