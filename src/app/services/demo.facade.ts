import {Injectable} from '@angular/core';
import {BehaviorSubject, combineLatest, forkJoin, Observable, of, Subscription} from 'rxjs';
import {Post, User} from '../app.component';
import {DemoService} from './demo.service';
import {catchError, debounceTime, filter, map, publishReplay, refCount, shareReplay, switchMap, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DemoFacade {

  activePostId$ = new BehaviorSubject<null|number>(null);

  private _error$ = new BehaviorSubject<string|null>(null);
  error$: Observable<string|null> = this._error$;
  private _isLoading$ = new BehaviorSubject(true);
  isLoading$: Observable<boolean> = this._isLoading$;

  private _activePost$ = new BehaviorSubject<Post|null>(null);
  activePost$: Observable<Post|null> = this._activePost$;
  private _activeUser$ = new BehaviorSubject<User|null>(null);
  activeUser$: Observable<User|null> = this._activeUser$;

  // region Multicast Avancé

  activePostWithUser$: Observable<PostWithUser> = combineLatest([
    this.activePost$,
    this.activeUser$,
  ]).pipe(
    filter(([post, user]) => !!post && !!user),
    tap(([post, user]) => console.log('Combining post and user...', post.id, user.id)),
    debounceTime(2000),
    map(([post, user]) => {
      return {...post, user};
    }),
    tap(res => console.log('Combined post with user', res)),
  );

  activePostWithUserShareReplay$: Observable<PostWithUser> = this.activePostWithUser$.pipe(
    // ShareReplay va transformer l'Observable en Observable "hot"
    // Il continuera d'émettre même si plus aucun Observer n'est actif
    shareReplay(1),
  );

  activePostWithUserPublishReplay$: Observable<PostWithUser> = this.activePostWithUser$.pipe(
    // Je ne veux pas trop rentrer dans les détails d'implémentation mais il existe un type particulier
    // d'Observable : les "ConnectableObservable"
    // Ces Observables ne deviennent actif que si on appelle leur fonction "connect" et se désactive sur "disconnect"
    // Ceci, peu importe le nombre d'Observers actifs
    // Ici nous n'appelons pas la fonction connect donc rien ne se produit
    publishReplay(1),
  );

  activePostWithUserPublishReplayRefCount$: Observable<PostWithUser> = this.activePostWithUser$.pipe(
    publishReplay(1),
    // L'opérateur refCount() permet d'appeler automatiquement connect et disconnect en fonction du nombre
    // d'Observers actifs
    // On notera que publishReplay lors d'une "reconnexion" va émettre la dernière valeur reçue lors de la "déconnexion"
    refCount(),
  );

  activePostWithUserShareReplayRefCount$: Observable<PostWithUser> = this.activePostWithUser$.pipe(
    // ShareReplay avec refCount va déconnecter l'Observable lorsqu'il n'y a plus d'Observer
    // Contrairement à publishReplay + refCount, lors d'une "reconnexion" la dernière valeur ne sera pas émise
    shareReplay({bufferSize: 1, refCount: true}),
  );
  // endregion

  constructor(
    private demoService: DemoService,
  ) {}

  loadActivePost() {
    return this.activePostId$.pipe(
      tap(postId => {
        this._isLoading$.next(!!postId);
        this._error$.next(null);
        this._activePost$.next(null);
        this._activeUser$.next(null);
      }),
      filter(postId => !!postId),
      switchMap(postId => {
        return this.demoService.getPost(postId).pipe(
          catchError(error => {
            console.error('getPost', error);
            this._error$.next('Une erreur est survenue lors de la récupération du post...');
            this._isLoading$.next(false);
            return of(null);
          })
        );
      }),
      filter(post => !!post),
      tap(post => this._activePost$.next(post)),
      switchMap(post => {
        return this.demoService.getUser(post.userId === 2 ? -1 : post.userId).pipe(
          catchError(error => {
            console.error('getUser', error);
            this._error$.next(`Une erreur est survenue lors de la récupération de l'utilisateur...`);
            this._isLoading$.next(false);
            return of(null);
          }),
        );
      }),
      tap(user => {
        this._activeUser$.next(user);
        this._isLoading$.next(false);
      }),
    );
  }

  retryLoadActivePost() {
    this.activePostId$.next(this.activePostId$.value);
  }

}

export interface PostWithUser extends Post {
  user: User;
}
